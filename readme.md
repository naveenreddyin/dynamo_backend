#### Installation and running the project
1. Clone this repository
2. Prerequisite is to have Python3 in the system
3. Using command line go inside the cloned repository
4. Install virtualenv or virtualenvwrapper or conda or pyenv and create a new virtual environment and activate it.
5. Than run `pip install -r requirements.txt` from inside the cloned repository
6. Than run `python manage.py migrate`
7. Finally start the server using the command `python manage.py runserver`
8. Run the tests and coverage report `coverage run --source='.' manage.py test && coverage report`, right now the code coverage is 95%
