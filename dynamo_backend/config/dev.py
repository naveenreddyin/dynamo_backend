from .base import *

"""
Add the apps which are only needed for development
"""
INSTALLED_APPS += [
    'django_extensions',
    'corsheaders',
]

# Override the ALLOWED_HOSTS for development environment only
ALLOWED_HOSTS = ['*',]

SWAGGER_SETTINGS = {
    'SECURITY_DEFINITIONS': {
        'basic': {
            'type': 'basic'
        }
    },
}

LOGIN_URL = 'rest_framework:login'
LOGOUT_URL = 'rest_framework:logout'