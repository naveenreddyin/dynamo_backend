from datetime import timedelta

from django.test import TestCase
from django.contrib.auth import get_user_model
from django.urls import reverse
from django.utils.timezone import now
from django.core.exceptions import ValidationError

from rest_framework.test import APITestCase, APIClient
from rest_framework.authtoken.models import Token

from api.models import Flight


class ApiAppTest(APITestCase):

    def setUp(self):
        self.User = get_user_model()
        self.User.objects.create_superuser('naveen', 
        'naveen@dispostable.com', 
        'naveen')
        Token.objects.create(user=self.User.objects.get(username='naveen'))
        self.client = APIClient()
        self.token_url = reverse('api_token_auth')
        self.register_url = reverse('api_register')
        self.flight_url = reverse('api_flight')

    def test_login_api_view(self):
        """
        This will be used to test login api
        1. Call to get should return 405, method not allowed
        2. Call with post methdo and should return 200, with token in data
        """
        
        # 1
        response = self.client.get(self.token_url)
        self.assertEqual(response.status_code, 405)
        # 2
        response = self.client.post(self.token_url, {'username': 'naveen', 
        'password': 'naveen'})
        self.assertEqual(response.status_code, 200)
        self.assertIsNotNone(response.data.get('token'))

    def test_user_register_api_view(self):
        """
        This test is for user registration through api.
        1. Call to GET method should return 405, method not allowed as we shall only be doing POST
        2. Call to POST methdo should return 201, and User object model should return two users,
         one being super user already
        """
        data = {
            "password": "TESTPASSWORD",
            "last_name": "testname",
            "first_name": "testname",
            "email": "testemail@dispostable.com"
        }
        # 1
        self.assertEqual(self.User.objects.all().count(), 1)
        response = self.client.get(self.register_url)
        self.assertEqual(response.status_code, 405)

        # 2
        response = self.client.post(self.register_url, data)
        self.assertEqual(response.status_code, 201)
        self.assertIsNotNone(response.data.get('pk' ))
        self.assertEqual(self.User.objects.all().count(), 2)

    def test_flight_model(self):
        """
        This test to test the Flight model for saving status of Flights.
        1. Create flight data and that should be checked the first one being in database. In this
        the data for scheduled date time is more than arrival date time, which should raise exception
        2. Create new test data and try to enter it, i.e with correct scheduled and arrival time and date.
        """
        # 1
        data = {
            "flight_name": "SAS 201",
            "flight_number": 12345,
            "scheduled_date_time": now()+timedelta(days=1),
            "eta_arrival_date_time": now()-timedelta(days=1),
            "departure_from": "Oslo",
            "destination": "Stockhol",
            "fare": 100,
            "flight_duration": 1
        }
        with self.assertRaises(ValidationError) as exception_context_manager:
            flight = Flight.objects.create(**data)

        # 2
        data = {
            "flight_name": "SAS 201",
            "flight_number": 12345,
            "scheduled_date_time": now()+timedelta(days=1),
            "eta_arrival_date_time": now()+timedelta(days=1),
            "departure_from": "Oslo",
            "destination": "Stockhol",
            "fare": 100,
            "flight_duration": 1
        }
        flight = Flight.objects.create(**data)
        self.assertEqual(Flight.objects.all().count(), 1)
        
    def test_flight_api_view(self):
        """
        This test is for flight api view, it will give 200 status for get request and for post 201 if all is good. 
        1. Try to GET the resource and it should return 200 status code but data should be none, 
        that is if user is logged in. 
        2. Try to create a resource using POST and it should return 400 as scheduled time is more than arrival time.
        3. Now create the resource using POST with normal clean data, should return 201
        4. You wont be able to create a Flight if you are not logged in, should return 401
        """
        # 1
        response = self.client.get(self.flight_url)
        self.assertEqual(response.status_code, 401)
        # login
        token = Token.objects.get(user__username='naveen')
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key) 
        response = self.client.get(self.flight_url)
        self.assertEqual(len(response.data), 0)
        self.assertEqual(response.status_code, 200)
        # 2
        data = {
            "flight_name": "SAS 201",
            "flight_number": 12345,
            "scheduled_date_time": now()+timedelta(days=1),
            "eta_arrival_date_time": now()-timedelta(days=1),
            "departure_from": "Oslo",
            "destination": "Stockhol",
            "fare": 100,
            "flight_duration": 1
        }
        response = self.client.post(self.flight_url, data)
        self.assertEqual(response.status_code, 400)
        # 3
        data = {
            "flight_name": "SAS 201",
            "flight_number": 12345,
            "scheduled_date_time": now()+timedelta(days=1),
            "eta_arrival_date_time": now()+timedelta(days=1),
            "departure_from": "Oslo",
            "destination": "Stockhol",
            "fare": 100,
            "flight_duration": 1
        }
        response = self.client.post(self.flight_url, data)
        self.assertEqual(response.status_code, 201)
        # 4
        self.client.logout()
        response = self.client.post(self.flight_url, data)
        self.assertEqual(response.status_code, 401)



        
