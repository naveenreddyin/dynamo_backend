from django.urls import path, include

from rest_framework.authtoken.views import obtain_auth_token
from rest_framework import routers
from rest_framework_swagger.views import get_swagger_view

from api.views import RegisterApiView, FlightListCreateApiView

schema_view = get_swagger_view(title='Dynamo API')

urlpatterns = [
    path('api-token-auth/', obtain_auth_token, name='api_token_auth'),
    path('register', RegisterApiView.as_view(), name='api_register'),
    path('flight', FlightListCreateApiView.as_view(), name='api_flight'),
    path('swagger/', schema_view),
]
