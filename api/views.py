from django.contrib.auth import get_user_model
from rest_framework import generics
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import AllowAny

from api.serializers import UserSerializer, FlightSerializer
from api.models import Flight


class RegisterApiView(generics.CreateAPIView):
    """
    This class deals with register a user with email, password, first_name and last_name fields.
    We shall be using basic inbuilt Django User model, of course one could have a custom user model
    as in real world.
    Permission class would be to allow any as default is set to isAuthenticated.
    There is a UserSerializer class to accomodate serliazing process.
    We override function post which allows us to create token for registering user. We could have
    done it in serliazer class too, but its part of segregation of responsibility.
    """
    permission_classes = {AllowAny,}
    serializer_class = UserSerializer

    def create(self, request, *args, **kwargs):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            if user:
                token = Token.objects.create(user=user)
                json = serializer.data
                json['pk'] = user.pk
                json['token'] = token.key
                return Response(json, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class FlightListCreateApiView(generics.ListCreateAPIView):
    queryset = Flight.objects.all()
    serializer_class = FlightSerializer

    def create(self, request, *args, **kwargs):
        serializer = FlightSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)