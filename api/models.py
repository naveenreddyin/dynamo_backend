from django.db import models
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ValidationError


class Flight(models.Model):
    flight_name = models.CharField(max_length=50)
    flight_number = models.PositiveIntegerField()
    scheduled_date_time = models.DateTimeField()
    eta_arrival_date_time = models.DateTimeField()
    departure_from = models.CharField(max_length=50)
    destination = models.CharField(max_length=50)
    fare = models.PositiveSmallIntegerField()
    flight_duration = models.PositiveSmallIntegerField()

    def clean(self, *args, **kwargs):
        if self.scheduled_date_time >= self.eta_arrival_date_time:
            raise ValidationError(_('Something wrong with scheduled and arrival date time.'))
        super().clean(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)