from django.contrib.auth.models import User
from rest_framework import serializers
 
from api.models import Flight

 
class UserSerializer(serializers.ModelSerializer):
    """
    This serializer class is to be used for registering the user.
    We set email same as to username, and password overriding create method.
    we did not set any password length field.
    We did not set any password confirm field, that could be done with client side logic, the
    basis of that goes with ethos of Python "we are all adults here", but it could be done.
    """
    class Meta:
        model = User
        fields = ('email', 'password', 'first_name', 'last_name',)
 
    def create(self, validated_data):
       password = validated_data.pop("password")
       username = validated_data.pop("email")
       user = User.objects.create(username=username, 
       email=username, 
       **validated_data)
       if password:
           user.set_password(password)
           user.save()
       return user


class FlightSerializer(serializers.ModelSerializer):
    """
    This serializer is for Flight model and its related api view.
    """
    class Meta:
        model = Flight
        fields = ('__all__')
    
    def validate(self, data):
        """
        Check that scheduled time is before finish.
        """
        if data['scheduled_date_time'] >= data['eta_arrival_date_time']:
            raise serializers.ValidationError("Scheduled time should be less than arrival time")
        return data